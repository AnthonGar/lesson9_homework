#include "BSNode.h"
#include <iostream>
#include <string>
#include <fstream>

//C-tor for the BSNode class.
BSNode::BSNode(std::string data) : _data(data), _left(0), _right(0), _count(1)
{
}

//Copy C-tor for the BSNode class.
BSNode::BSNode(const BSNode& other)
{
	*this = other;
}

//D-tor for the BSNode class.
BSNode::~BSNode()
{
	//If there is something to delete on the left.
	if (_left)
	{
		delete _left;
	}
	//If there is something to delete on the right.
	if (_right)
	{
		delete _right; 
	}
}

//get depth from the root.
int BSNode::getDepth(const BSNode& root) const
{
	return getCurrNodeDistFromInputNode(&root);
}

//Getter for _data
std::string BSNode::getData() const
{
	return _data;
}

//Getter for _left
BSNode* BSNode::getLeft() const
{
	return _left;
}

//Getter for _right
BSNode* BSNode::getRight() const
{
	return _right;
}

//Checks if the node is a leaf.
bool BSNode::isLeaf() const
{
	//If there is no sons on the left and right that means that the node is a leaf.
	return (_left == 0 && _right == 0);
}

//Deep Copy '=' operator for the BSNode class.
BSNode& BSNode::operator=(const BSNode& other)
{
	//Delete existing tree (if there is something to delete).
	if (_left)
	{
		delete _left;
	}
	if (_right)
	{
		delete _right;
	}

	//Init new but with other's data.
	_data = other._data;
	_left = 0;
	_right = 0;

	//If there is something left to copy in the tree.
	if (other._left)
	{
		_left = new BSNode(*other._left);
	}

	if (other._right)
	{
		_right = new BSNode(*other._right);
	}
	//Keeping the count.
	_count = other._count;
	return *this;
}

//Calculates the hieght of the tree.
int BSNode::getHeight() const
{
	int returnVal;

	// if the root is a leaf so the height is 1
	if (isLeaf())
	{
		returnVal = 1;
	}
	else
	{
		int rightHeight = 0;
		int leftHeight = 0;

		//checking height of the left sub tree
		if (_left)
		{
			leftHeight = _left->getHeight();
		}

		//checking height of the right sub tree
		if (_right)
		{
			rightHeight = _right->getHeight();
		}

		//checking whitch sub tree is deeper.
		if (rightHeight >= leftHeight)
		{
			returnVal = rightHeight + 1; //+ root
		}
		else
		{
			returnVal = leftHeight + 1; //+ root
		}
	}

	return returnVal;
}

//This function will insert a value to the right spot on the tree.
void BSNode::insert(std::string value)
{
	//Check there the value has to go.
	if (value < this->_data)
	{
		//Check if there is a son (so we must go deeper) else we need to create a new leaf. 
		if (_left)
		{
			_left->insert(value);
		}
		else
		{
			_left = new BSNode(value);
		}
	}
	else if (value> this->_data)
	{
		//Check if there is a son (so we must go deeper) else we need to create a new leaf.
		if (_right)
		{
			_right->insert(value);
		}
		else
		{
			_right = new BSNode(value);
		}
	}
	//Else we have the value already writen so we add one to the count.
	else
	{
		this->_count++;
	}
}

//This func will search for a value in the tree, if there is a value --> true, else --> false.
bool BSNode::search(std::string value) const
{
	bool returnVal = false;

	// if the current node has the value
	if (_data == value)
	{
		returnVal = true;
	}
	//Check where we have to go
	else if (_data > value) 
	{
		//If there is where to go.
		if (_left)
		{
			returnVal = _left->search(value);
		}
	}
	//If there is where to go.
	else if (_right)
	{
		returnVal = _right->search(value);
	}

	return returnVal;
}

//This func will print the tree.
void BSNode::printNodes() const
{
	//If there is where to go (and left is bigger so it must be printed first).
	if (_left)
	{
		_left->printNodes();
	}
	
	//Print
	std::cout << _data << " " << _count << std::endl;

	//If there is where to go (and right is smaller so it must be printed last).
	if (_right)
	{
		_right->printNodes();
	}

}

//calculated the depth between "this" and other node.
int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	//If they are the same.
	if (this == node)
	{
		return 0;
	}
	//checking where we have to go.
	else if (node->_data >= this->_data)
	{
		//if there is where to go
		if (node->_left)
		{
			return getCurrNodeDistFromInputNode(node->_left) + 1;
		}

	}
	//if there is where to go
	else if (node->_right)
	{
		return getCurrNodeDistFromInputNode(node->_right) + 1; // recursive call on right son
	}

	//error
	return -1;
}

