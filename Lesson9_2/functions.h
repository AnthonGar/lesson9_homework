#pragma once

//ex2 part 1
template <class T>
int compare(T a, T b)
{
	if (a > b)
		return -1;
	else if (b > a)
		return 1;
	else
		return 0;
}

//ex2 part 2
template <class T>
void bubbleSort(T* a, int size)
{
	bool swapped = false;
	int iteration = 0;
	do
	{
		swapped = false;
		for (int i = 1; i < size - iteration; i++)
		{
			//If a swap is needed
			if (a[i - 1] > a[i])
			{
				std::swap(a[i - 1], a[i]);
				swapped = true;
			}
		}
		iteration++;
		//When swapped will be false it means that we went througth 
		//a whole iteration and nothing changed
		//whitch means that its sorted.
	} while (swapped);
}

//ex2 part 3
template <class T>
void printArray(T* arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << arr[i] << " ";
	}
}