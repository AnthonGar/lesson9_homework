#include "functions.h"
#include <iostream>

//We get a C-tor and D-tor for free! (its just an int...)
class bob {
public:
	//Use out side the class's scope but still access the members
	friend std::ostream& operator<<(std::ostream& os, const bob& dt);
	bob(int n0t);


public:
	int _funny;
};

bob::bob(int n0t) : _funny(n0t)
{

}

//Thank you StackOverFlow!
std::ostream& operator<<(std::ostream& os, const bob& a)
{
	os << a._funny;
	return os;
}

//Other functions:
bool operator==(const bob& left, const bob& right)
{
	if (left._funny == right._funny)
		return true; else return false;

}

bool operator!=(const bob& left, const bob& right)
{
	// != is the oppsite of ==
	return !operator==(left, right);
}

bool operator< (const bob& left, const bob& right)
{
	if (left._funny < right._funny)
		return true;
	else
		return false;
}

bool operator> (const bob& left, const bob& right)
{
	// < is the oppsite of >
	return  !operator< (left, right);
}

bool operator<=(const bob& left, const bob& right)
{
	//<= same as: < || ==
	if (operator<(left, right) || operator== (left, right))
		return true;
	else
		return false;
}

bool operator>=(const bob& left, const bob& right)
{
	// >= same as: > || ==
	if (operator>(left, right) || operator== (left, right))
		return true;
	else
		return false;
}





int main() {

	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	std::cout << compare<char>('a', 'b') << std::endl;
	std::cout << compare<char>('b', 'a') << std::endl;
	std::cout << compare<char>('a', 'a') << std::endl;

	std::cout << compare<bob>(1, 2) << std::endl;
	std::cout << compare<bob>(2, 1) << std::endl;
	std::cout << compare<bob>(1, 1) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	const int arr_size2 = 5;
	char charArr[arr_size2] = { 'e', 'd', 'c', 'b', 'a' };
	bubbleSort<char>(charArr, arr_size2);
	for (int i = 0; i < arr_size2; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	const int arr_size3 = 5;
	bob bobArr[arr_size3] = { 56, 67, 12, 23, 21 };
	bubbleSort<bob>(bobArr, arr_size3);
	for (int i = 0; i < arr_size3; i++) {
		std::cout << bobArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;
	printArray<char>(charArr, arr_size2);
	std::cout << std::endl;
	printArray<bob>(bobArr, arr_size2);
	std::cout << std::endl;

	system("pause");
	return 1;
}